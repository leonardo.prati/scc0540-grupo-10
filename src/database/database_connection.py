import json
import os
from typing import Union

import cx_Oracle as oracle

class DatabaseConnection:
    """Class to handle database connection. It's a singleton class,
    thus all instances will share the same connection.
    
    This behavior is ensured by the class attributes.
    """
    
    is_connected = False
    connection = None

    def load_database_config(self) -> dict:
        """Load database configuration from a JSON file pointed by
        the environment variable DATABASE_CONFIG_FILE.

        Returns:
            dict: Database configuration.
        """
        with open(os.environ.get('DATABASE_CONFIG_FILE', ''), 'r') as f:
            db_config = json.load(f)
        return db_config

    def connect(self) -> Union[oracle.Connection, None]:
        """Connect to the database.
        
        If the connection is already established, it will return the
        existing connection.
        
        Otherwise, it will try to connect to the database and return
        the new connection.

        If the connection fails, prints the error and returns None.

        Returns:
            Union[oracle.Connection, None]: Database connection, or None if
            connection fails.
        """
        if not self.is_connected:
            try:
                config = self.load_database_config()
                dsn = oracle.makedsn(
                    config['host'],
                    port=config['port'],
                    service_name=config['service_name']
                )
                self.connection = oracle.connect(
                    user=config['user'],
                    password=config['password'],
                    dsn=dsn,
                    encoding="UTF-8"
                )
                self.is_connected = True
            except oracle.DatabaseError as e:
                print("Database connection error: ", e)
                return None
        return self.connection
