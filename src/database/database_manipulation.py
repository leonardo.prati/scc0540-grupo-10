from datetime import datetime
from cx_Oracle import IntegrityError, InternalError, DatabaseError, InterfaceError

from src.database.database_connection import DatabaseConnection


def get_alunos(query_field: str, query_parameter: str):
    """Search for people of type "Aluno" in the database.

    Args:
        query_field (str): The field used in search (Nome, CPF, Email).
        query_parameter (str): The value to search for.

    Raises:
        ValueError: If the query field is invalid.
        InternalError: If there is an internal error in the database.
        DatabaseError: If there is an error in the database.
        InterfaceError: If there is an error in the connection with the database.
        Exception: If there is an unknown error.

    Returns:
        List[Dict]: List of people of type "Aluno" found in the database with
        the given query parameter.
    """
        
    query = "SELECT * FROM PESSOA WHERE TIPO = 'ALUNO'"
    if query_field == "Nome":
        # Add % to the query parameter to make it a LIKE query.
        query_parameter += "%"
        query += " AND NOME_COMPLETO LIKE UPPER(:query_parameter)"
    elif query_field == "CPF":
        query += " AND CPF = :query_parameter"
    elif query_field == "Email":
        query += " AND EMAIL = :query_parameter"
    else:
        raise ValueError("Invalid query field.")
    
    with DatabaseConnection().connect().cursor() as cursor:
        try:
            results = cursor.execute(query, query_parameter=query_parameter)
            alunos = []
            for result in results.fetchall():
                alunos.append(
                    {
                        "CPF": result[0],
                        "Nome Completo": result[1],
                        "Data Nascimento": result[2].strftime("%d/%m/%Y"),
                        "Rua": result[3],
                        "Número": result[4],
                        "Bairro": result[5],
                        "Complemento": result[6],
                        "Cidade": result[7],
                        "Estado": result[8],
                        "Email": result[10],
                        "Celular": result[9],
                        "Telefone Contato": result[11],
                        "Nivel Formação": result[12]
                    }
                )
        except DatabaseError as e:
            raise DatabaseError("Erro de banco de dados. Contate o administrador.") from e
        except InternalError as e:
            raise InternalError("Erro interno. Tente novamente mais tarde, e contate o administrador se o erro persistir") from e
        except InterfaceError as e:
            raise InterfaceError("Erro de conexão com o banco de dados. Contate o administrador.") from e
    return alunos

def create_aluno(
    cpf: str,
    nome_completo: str,
    data_nasc: str,
    rua: str,
    numero: str,
    bairro: str,
    complemento: str,
    cidade: str,
    estado: str,
    celular: str,
    tel_contato: str,
    email: str,
    nivel_formacao: str
):
    """Create a new person of type "Aluno" in the database.
    
    Args:
        cpf (str): CPF of the person.
        nome_completo (str): Full name of the person.
        data_nasc (str): Date of birth of the person in DD/MM/YYYY format.
        rua (str): Street of the person's address.
        numero (str): Number of the person's address.
        bairro (str): Neighborhood of the person's address.
        complemento (str): Complement of the person's address.
        cidade (str): City of the person's address.
        estado (str): State of the person's address in short representation (2 chars).
        celular (str): Cellphone number of the person.
        tel_contato (str): Contact phone number of the person.
        email (str): Email of the person.
        nivel_formacao (str): Level of education of the person.
    
    Returns:
        bool: True if the person was created successfully.
    
    Raises:
        IntegrityError: If the person already exists in the database.
        InternalError: If there is an internal error in the database.
        DatabaseError: If there is an error in the database.
        InterfaceError: If there is an error in the connection with the database.
        Exception: If there is an unknown error.
    """
    query = """
        INSERT INTO PESSOA (
            CPF,
            NOME_COMPLETO,
            DATA_NASC,
            RUA,
            NUMERO,
            BAIRRO,
            COMPLEMENTO,
            CIDADE,
            ESTADO,
            CELULAR,
            TEL_CONTATO,
            EMAIL,
            NIVEL_FORMACAO,
            TIPO   
        )
        VALUES (
            :cpf,
            :nome_completo,
            TO_DATE(:data_nasc, 'DD/MM/YYYY'),
            :rua,
            :numero,
            :bairro,
            :complemento,
            :cidade,
            :estado,
            :celular,
            :tel_contato,
            :email,
            :nivel_formacao,
            'ALUNO'
        )
    """
    try:
        connection = DatabaseConnection().connect()
        with connection.cursor() as cursor:
            cursor.execute(
                query,
                cpf=cpf,
                nome_completo=nome_completo,
                data_nasc=data_nasc,
                rua=rua,
                numero=numero,
                bairro=bairro,
                complemento=complemento,
                cidade=cidade,
                estado=estado,
                celular=celular,
                tel_contato=tel_contato,
                email=email,
                nivel_formacao=nivel_formacao
            )
            cursor.execute("INSERT INTO ALUNO (CPF) VALUES (:cpf)", cpf=cpf)
            connection.commit()
            return True        
    except IntegrityError as e:
        print(e, type(e), flush=True)
        raise e
    except InternalError as e:
        print(e, type(e), flush=True)
        raise e
    except DatabaseError as e:
        print(e, type(e), flush=True)
        raise e
    except InterfaceError as e:
        print(e, type(e), flush=True)
        raise e
    except Exception as e:
        print(e, type(e), flush=True)
        raise Exception("Erro desconhecido. Contate o administrador.") from e
