FROM python:3.9

RUN apt-get update && apt-get upgrade -y

# Instala o Oracle Instant Client e suas dependências
RUN apt install libaio1
RUN cd /opt/
RUN mkdir /opt/oracle
WORKDIR /opt/oracle
RUN wget https://download.oracle.com/otn_software/linux/instantclient/214000/instantclient-basic-linux.x64-21.4.0.0.0dbru.zip
RUN unzip instantclient-basic-linux.x64-21.4.0.0.0dbru.zip
RUN sh -c "echo /opt/oracle/instantclient_21_4 > /etc/ld.so.conf.d/oracle-instantclient.conf"
RUN ldconfig

# Instala as dependências do projeto
COPY requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt

# Copia código fonte
COPY ./src /app
COPY ./pages /app/pages
COPY Home_Page.py /app

# Sobe a aplicação
CMD ["python3", "-m", "streamlit", "run", "Home_Page.py"]