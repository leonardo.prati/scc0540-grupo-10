-- Selecionar, para cada funcionário administrativo, o nome do funcionário, sua data de contratação, o número de
-- doações financeiras que ele obteve junto às empresas, o valor total dessas doações, número de doações de equipamento
-- que ele já obteve junto às empresas, e o valor nominal total dessas doações
SELECT F.nome, F.data_contratacao, COUNT(*), SUM(DF.valor), COUNT(*), SUM(DE.valor_nominal) FROM funcionario F
    LEFT JOIN doacaoFinanceira DF
        ON F.cpf = DF.administrativo
    LEFT JOIN doacaoEquipamentos DE 
        ON F.cpf = DE.administrativo
    GROUP BY F.nome, F.data_contratacao;

-- Selecionar o nome_fantasia, cnpj, telefone comercial e nome do representante de todas as instituições apoiadoras
-- que nunca apoiaram nenhum curso da área de 'GESTAO'
SELECT I.nome_fantasia, I.cnpj, I.telefone_comercial, I.nome_repr FROM instituicaoApoiadora I
    LEFT JOIN doacaoFinanceira DF
        ON I.cnpj = DF.instituicao
    LEFT JOIN doacaoEquipamentos DE
        ON I.cnpj = DE.instituicao
    JOIN mentoria M
        ON DE.administrativo = M.administrativo AND DE.data_inicio = M.data_inicio AND DE.aluno = M.aluno
    JOIN oferecimento O
        ON M.aluno = O.aluno
    WHERE O.curso NOT IN (SELECT codigo FROM curso WHERE UPPER(area) = 'GESTAO');

-- Selecionar o nome, idade, telefone e media_atividades de todos os alunos matriculados como alunos transforma
-- que foram aprovados em cursos da sub_area 'REACT' com média de atividades de pelo menos 7.5 e que não possuem mentoria
SELECT P.nome, P.idade, P.tel_contato, AVG(AA.media_atividades) FROM pessoa P
    JOIN matricula M
        ON P.cpf = M.aluno
    JOIN oferecimento O
        ON M.curso = O.curso AND M.numero = O.numero AND M.data_matricula = O.data
    JOIN atividade_aluno AA
        ON P.cpf = AA.aluno AND O.curso = AA.curso AND O.numero = AA.numero AND O.data = AA.data
    LEFT JOIN mentoria ME
        ON P.cpf = ME.aluno
    WHERE UPPER(O.e_transforma) = 'S' AND O.curso IN (SELECT codigo FROM curso WHERE UPPER(sub_area) = 'REACT') AND ME.aluno IS NULL
    GROUP BY P.nome, P.idade, P.telefone
    HAVING AVG(AA.media_atividades) >= 7.5;

-- Selecionar, por ano e por mês, o número de alunos transforma que concluiram curso, juntamente com a nota média
-- que estes alunos obtiveram nas atividades, ordenado por mês e ano
SELECT TO_CHAR(O.data_fim, 'MM'), TO_CHAR(O.data_fim, 'YYYY'), COUNT(*), AVG(AA.media_atividades) FROM matricula M
    JOIN oferecimento O
        ON M.curso = O.curso AND M.numero = O.numero AND M.data_matricula = O.data
    JOIN atividade_aluno AA
        ON O.curso = AA.curso AND O.numero = AA.numero AND O.data = AA.data
    WHERE UPPER(M.aprovado) = 'S' AND UPPER(O.e_transforma) = 'S'
    GROUP BY TO_CHAR(O.data_fim, 'MM'), TO_CHAR(O.data_fim, 'YYYY')
    ORDER BY TO_CHAR(O.data_fim, 'MM'), TO_CHAR(O.data_fim, 'YYYY');


-- Selecionar nome, idade e telefone de contato dos alunos que concluiram todos os cursos da área de 'GESTAO'
SELECT P.nome, P.idade, P.tel_contato FROM pessoa P
    WHERE NOT EXISTS (
        SELECT curso FROM matricula M JOIN curso C ON M.curso = C.codigo
            WHERE UPPER(M.aprovado) = 'S' AND UPPER(C.area) = 'GESTAO' AND P.cpf = M.aluno
    ) MINUS (
        SELECT codigo FROM curso WHERE UPPER(area) = 'GESTAO'
    );
