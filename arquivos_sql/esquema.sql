CREATE TABLE pessoa (
    cpf VARCHAR(11) NOT NULL,
    nome_completo VARCHAR(64) NOT NULL,
    data_nasc DATE NOT NULL,
    rua VARCHAR(64) NOT NULL,
    numero VARCHAR(8) NOT NULL,
    bairro VARCHAR(32) NOT NULL,
    complemento VARCHAR(32) NOT NULL,
    cidade VARCHAR(32) NOT NULL,
    estado VARCHAR(2) NOT NULL,
    celular VARCHAR(11) NOT NULL,
    tel_contato VARCHAR(11),
    email VARCHAR(64) NOT NULL,
    nivel_formacao VARCHAR(16),
    tipo VARCHAR(13),
    CONSTRAINT pk_pessoa PRIMARY KEY (cpf),
    CONSTRAINT un_email UNIQUE(email), 
    CONSTRAINT ck_pessoa_cpf CHECK (REGEXP_LIKE(cpf, '[0-9]{11}$')),
    CONSTRAINT ck_pessoa_celular CHECK (REGEXP_LIKE(celular, '[0-9]{11}$')),
    CONSTRAINT ck_pessoa_tel_contato CHECK (REGEXP_LIKE(tel_contato, '[0-9]{11}$')),
    CONSTRAINT ck_pessoa_email CHECK (REGEXP_LIKE(email, '[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$')),
    CONSTRAINT ck_pessoa_nivel_formacao CHECK (UPPER(nivel_formacao) IN ('FUNDAMENTAL', 'MEDIO', 'SUPERIOR')),
    CONSTRAINT ck_pessoa_tipo CHECK (UPPER(tipo) IN ('ALUNO', 'INSTRUTOR', 'COORDENADOR', 'FUNCIONARIO','ADMINISTRADOR'))
);

CREATE TABLE funcionario (
    pessoa VARCHAR(11) NOT NULL,
    data_contratacao DATE NOT NULL,
    CONSTRAINT pk_funcionario PRIMARY KEY (pessoa),
    CONSTRAINT fk_funcionario_pessoa FOREIGN KEY (pessoa) REFERENCES pessoa (cpf) ON DELETE CASCADE
);

CREATE TABLE instrutor (
    funcionario VARCHAR(11) NOT NULL,
    area_especialidade VARCHAR(32) NOT NULL,
    CONSTRAINT pk_instrutor PRIMARY KEY (funcionario),
    CONSTRAINT fk_instrutor_funcionario FOREIGN KEY (funcionario) REFERENCES funcionario (pessoa) ON DELETE CASCADE
);

CREATE TABLE coordenador (
    funcionario VARCHAR(11) NOT NULL,
    area_especialidade VARCHAR(32) NOT NULL,
    CONSTRAINT pk_coordenador PRIMARY KEY (funcionario),
    CONSTRAINT fk_coordenador_funcionario FOREIGN KEY (funcionario) REFERENCES funcionario (pessoa) ON DELETE CASCADE
);

CREATE TABLE administrador (
    funcionario VARCHAR(11) NOT NULL,
    CONSTRAINT pk_administrador PRIMARY KEY (funcionario),
    CONSTRAINT fk_administrador_funcionario FOREIGN KEY (funcionario) REFERENCES funcionario (pessoa) ON DELETE CASCADE
);

CREATE TABLE aluno (
    pessoa VARCHAR(11) NOT NULL,
    mentor VARCHAR(11) NOT NULL,
    CONSTRAINT pk_aluno PRIMARY KEY (pessoa),
    CONSTRAINT fk_aluno_pessoa FOREIGN KEY (pessoa) REFERENCES pessoa (cpf) ON DELETE CASCADE,
    CONSTRAINT fk_mentor FOREIGN KEY (mentor) REFERENCES administrador (funcionario) ON DELETE CASCADE
);

CREATE TABLE curso (
    codigo VARCHAR(8) NOT NULL,
    nome VARCHAR(32) NOT NULL,
    area VARCHAR(32) NOT NULL,
    sub_area VARCHAR(32) NOT NULL,
    carga_horaria NUMBER NOT NULL,
    valor NUMBER NOT NULL,
    coordenador VARCHAR(11),
    CONSTRAINT pk_curso PRIMARY KEY (codigo),
    CONSTRAINT fk_curso_coordenador FOREIGN KEY (coordenador) REFERENCES coordenador (funcionario) ON DELETE CASCADE,
    CONSTRAINT ck_curso_carga_horaria CHECK (carga_horaria > 0)
);

CREATE TABLE materialApoio (
    curso VARCHAR(8) NOT NULL,
    nome VARCHAR(32) NOT NULL,
    CONSTRAINT pk_material_apoio PRIMARY KEY (curso, nome),
    CONSTRAINT fk_material_apoio_curso FOREIGN KEY (curso) REFERENCES curso (codigo) ON DELETE CASCADE
);

CREATE TABLE turma (
    curso VARCHAR(8) NOT NULL,
    numero NUMBER NOT NULL,
    data_inicio DATE NOT NULL,
    data_fim DATE NOT NULL,
    total_vagas NUMBER NOT NULL,
    vagas_transforma NUMBER NOT NULL,
    vagas_padrao_livres NUMBER NOT NULL,
    vagas_transforma_livres NUMBER NOT NULL,
    instrutor VARCHAR(11) NOT NULL,
    CONSTRAINT pk_turma PRIMARY KEY (curso, numero),
    CONSTRAINT fk_turma_curso FOREIGN KEY (curso) REFERENCES curso (codigo) ON DELETE CASCADE, 
    CONSTRAINT fk_instrutor FOREIGN KEY (instrutor) REFERENCES instrutor (funcionario) ON DELETE CASCADE,
    CONSTRAINT ck_turma_total_vagas CHECK (total_vagas > 0),
    CONSTRAINT ck_turma_vagas_transforma CHECK (vagas_transforma_livres <= vagas_transforma),
    CONSTRAINT ck_turma_vagas_padrao_livres CHECK (vagas_padrao_livres <= total_vagas - vagas_transforma)
);

CREATE TABLE aula (
    curso VARCHAR(8) NOT NULL,
    turma NUMBER NOT NULL,
    dia_semana VARCHAR(9) NOT NULL,
    horario TIMESTAMP NOT NULL,
    CONSTRAINT pk_aula PRIMARY KEY (curso, turma, dia_semana, horario),
    CONSTRAINT fk_aula_turma FOREIGN KEY (curso, turma) REFERENCES turma (curso, numero) ON DELETE CASCADE
);

CREATE TABLE oferecimento (
    instrutor VARCHAR(11) NOT NULL,
    curso VARCHAR(8) NOT NULL,
    turma NUMBER NOT NULL,
    data DATE NOT NULL,
    CONSTRAINT pk_oferecimento PRIMARY KEY (instrutor, curso, turma, data),
    CONSTRAINT fk_oferecimento_turma FOREIGN KEY (curso, turma) REFERENCES turma (curso, numero) ON DELETE CASCADE,
    CONSTRAINT fk_oferecimento_instrutor FOREIGN KEY (instrutor) REFERENCES instrutor (funcionario) ON DELETE CASCADE
);

CREATE TABLE matricula(
    aluno VARCHAR(11) NOT NULL,
    curso VARCHAR(8) NOT NULL,
    turma NUMBER NOT NULL,
    data DATE NOT NULL,
    e_transforma CHAR NOT NULL,
    valor NUMBER NOT NULL,
    pago CHAR DEFAULT 'N' NOT NULL,
    media_atividades NUMBER,
    concluido CHAR NOT NULL,
    aprovado CHAR,
    CONSTRAINT pk_matricula PRIMARY KEY (aluno, curso, turma, data),
    CONSTRAINT fk_matricula_aluno FOREIGN KEY (aluno) REFERENCES aluno (pessoa) ON DELETE CASCADE,
    CONSTRAINT fk_matricula_turma FOREIGN KEY (curso, turma) REFERENCES turma (curso, numero) ON DELETE CASCADE,
    CONSTRAINT ck_matricula_e_transforma CHECK (UPPER(e_transforma) IN ('S', 'N')),
    CONSTRAINT ck_matricula_pago CHECK (UPPER(pago) IN ('S', 'N')),
    CONSTRAINT ck_matricula_concluido CHECK (UPPER(concluido) IN ('S', 'N')),
    CONSTRAINT ck_matricula_aprovado CHECK (UPPER(aprovado) IN ('S', 'N')),
    CONSTRAINT ck_matricula_valor CHECK (valor >= 0),
    CONSTRAINT ck_matricula_media_atividades CHECK (media_atividades >= 0 AND media_atividades <= 10)
);

CREATE TABLE atividade (
    instrutor VARCHAR(11) NOT NULL,
    curso VARCHAR(8) NOT NULL,
    turma NUMBER NOT NULL,
    data DATE NOT NULL,
    nome VARCHAR(32) NOT NULL,
    CONSTRAINT pk_atividade PRIMARY KEY (instrutor, curso, turma, data, nome),
    CONSTRAINT fk_atividade_oferecimento FOREIGN KEY (instrutor, curso, turma, data) REFERENCES oferecimento (instrutor, curso, turma, data) ON DELETE CASCADE
);

CREATE TABLE realizaAtividade (
    aluno VARCHAR(11) NOT NULL,
    instrutor VARCHAR(11) NOT NULL,
    curso VARCHAR(8) NOT NULL,
    turma NUMBER NOT NULL,
    data DATE NOT NULL,
    atividade VARCHAR(32) NOT NULL,
    data_realizacao DATE NOT NULL,
    nota NUMBER,
    CONSTRAINT pk_realiza_atividade PRIMARY KEY (aluno, instrutor, curso, turma, data, atividade, data_realizacao),
    CONSTRAINT fk_realiza_atividade_atividade FOREIGN KEY (instrutor, curso, turma, data, atividade) REFERENCES atividade (instrutor, curso, turma, data, nome) ON DELETE CASCADE,
    CONSTRAINT fk_realiza_atividade_aluno FOREIGN KEY (aluno) REFERENCES aluno (pessoa) ON DELETE CASCADE,
    CONSTRAINT ck_realiza_atividade_nota CHECK (nota >= 0 AND nota <= 10)
);

CREATE TABLE mentoria (
    aluno VARCHAR(11) NOT NULL,
    mentor VARCHAR(11) NOT NULL,
    data_inicio DATE NOT NULL,
    data_termino DATE,
    gerou_contratacao CHAR DEFAULT 'N' NOT NULL,
    CONSTRAINT pk_mentoria PRIMARY KEY (aluno, mentor, data_inicio),
    CONSTRAINT fk_mentoria_aluno FOREIGN KEY (aluno) REFERENCES aluno (pessoa) ON DELETE CASCADE,
    CONSTRAINT fk_mentoria_administrador FOREIGN KEY (mentor) REFERENCES administrador (funcionario) ON DELETE CASCADE,
    CONSTRAINT ck_mentoria_gerou_contratacao CHECK (UPPER(gerou_contratacao) IN ('S', 'N'))
);

CREATE TABLE instituicaoApoiadora (
    cnpj VARCHAR(14) NOT NULL,
    nome_fantasia VARCHAR(64) NOT NULL,
    telefone_comercial VARCHAR(11) NOT NULL,
    rua VARCHAR(64) NOT NULL,
    numero NUMBER NOT NULL,
    bairro VARCHAR(32) NOT NULL,
    cidade VARCHAR(32) NOT NULL,
    estado VARCHAR(2) NOT NULL,
    complemento VARCHAR(32),
    nome_repr VARCHAR(64) NOT NULL,
    telefone_repr VARCHAR(11) NOT NULL,
    tipo CHAR(7) NOT NULL,
    CONSTRAINT pk_instituicao_apoiadora PRIMARY KEY (cnpj),
    CONSTRAINT ck_instituicao_apoiadora_tipo CHECK (UPPER(tipo) IN ('PUBLICA', 'PRIVADA'))
);

CREATE TABLE doacaoEquipamentos (
    data DATE NOT NULL, 
    instituicao VARCHAR(14) NOT NULL,
    mentor VARCHAR(11) NOT NULL,
    data_inicio DATE NOT NULL,
    aluno VARCHAR(11) NOT NULL,
    valor_nom_total NUMBER NOT NULL,
    CONSTRAINT pk_doacao_equipamentos PRIMARY KEY (data, instituicao, mentor, data_inicio, aluno),
    CONSTRAINT fk_doacao_equipamentos_instituicao FOREIGN KEY (instituicao) REFERENCES instituicaoApoiadora (cnpj) ON DELETE CASCADE,
    CONSTRAINT fk_doacao_equipamentos_mentoria FOREIGN KEY (mentor, data_inicio, aluno) REFERENCES mentoria (mentor, data_inicio, aluno) ON DELETE CASCADE
);

CREATE TABLE equipamentoDoado (
    cnpj VARCHAR(14) NOT NULL,
    data DATE NOT NULL,
    administrador VARCHAR(11) NOT NULL,
    data_inicio DATE NOT NULL,
    aluno VARCHAR(11) NOT NULL,
    nome VARCHAR(32) NOT NULL,
    descricao VARCHAR(64) NOT NULL,
    quantidade NUMBER NOT NULL,
    valor_nominal NUMBER NOT NULL,
    CONSTRAINT pk_equipamento_doado PRIMARY KEY (cnpj, data, nome, administrador, data_inicio, aluno),
    CONSTRAINT fk_equipamento_doado_doacao FOREIGN KEY (cnpj, data, administrador, data_inicio, aluno) REFERENCES doacaoEquipamentos (instituicao, data, mentor, data_inicio, aluno) ON DELETE CASCADE
);

CREATE TABLE doacaoFinanceira (
    instituicao VARCHAR(14) NOT NULL,
    administrador VARCHAR(11) NOT NULL,
    aluno VARCHAR(11) NOT NULL,
    curso VARCHAR(8) NOT NULL,
    turma NUMBER NOT NULL,
    data_matricula DATE NOT NULL,
    data DATE NOT NULL,
    valor NUMBER NOT NULL,
    CONSTRAINT pk_doacao_financeira PRIMARY KEY (instituicao, aluno, curso, turma, data_matricula, data),
    CONSTRAINT fk_doacao_financeira_instituicao FOREIGN KEY (instituicao) REFERENCES instituicaoApoiadora (cnpj) ON DELETE CASCADE,
    CONSTRAINT fk_doacao_financeira_oferecimento FOREIGN KEY (aluno, curso, turma, data_matricula) REFERENCES matricula (aluno, curso, turma, data) ON DELETE CASCADE,
    CONSTRAINT fk_doacao_financeira_mentoria FOREIGN KEY (administrador) REFERENCES administrador (funcionario) ON DELETE CASCADE
);
