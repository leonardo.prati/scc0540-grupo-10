import streamlit as st

# Set page name to Home
st.title("SCC0540 - Projeto Final")
st.subheader("Autores")
st.table(
    [
        {
            "Nome": "Leonardo Prati",
            "NUSP": "8300079",
            "Email": "leonardo.prati@usp.br"
        },
        {
            "Nome": "Rafael Oliver",
            "NUSP": "9436166",
            "Email": "rafael.oliver.cardoso@usp.br"
        },
        {
            "Nome": "Michelle Gmurczyk",
            "NUSP": "",
            "Email": ""
        }
    ]
)

