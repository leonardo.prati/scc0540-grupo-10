from dataclasses import asdict

import streamlit as st

from cx_Oracle import InternalError, DatabaseError, InterfaceError

from src.database.database_manipulation import get_alunos

def show_results(query_field, query_parameter):
    try:
        alunos = get_alunos(query_field, query_parameter)
        st.table(alunos)
    except InternalError as e:
        st.error("Erro interno. Tente novamente mais tarde, e contate o administrador se o erro persistir")
    except DatabaseError as e:
        st.error("Erro de banco de dados. Contate o administrador.")
    except InterfaceError as e:
        st.error("Erro de conexão com o banco de dados. Contate o administrador.")
    except ValueError as e:
        st.error("Campo de busca inválido. Tente novamente.")
    except Exception as e:
        st.error("Erro desconhecido. Contate o administrador.")
        

st.title("Buscar Aluno")

col1, col2, col3 = st.columns([2, 5, 1])
query_field = col1.selectbox("Buscar aluno pelo campo:", ["Nome", "CPF", "Email", "Todos os alunos"])
query_parameter = col2.text_input("com valor:")

col3.button("Buscar", on_click=show_results(query_field, query_parameter))