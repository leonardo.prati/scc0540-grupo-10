import streamlit as st

from cx_Oracle import IntegrityError, InternalError, DatabaseError, InterfaceError

from src.database.database_manipulation import create_aluno


def parse_nivel_formacao(nivel_formacao: str) -> str:
    """Converts the string representation of the level of education to the
    database expected representation.

    Args:
        nivel_formacao (str): The level of education in the form format. 

    Returns:
        str: The level of education in the database format.
    """
    if nivel_formacao == "Ensino Fundamental":
        return "ENSINO_FUNDAMENTAL"
    if nivel_formacao == "Ensino Médio":
        return "MEDIO"
    if nivel_formacao == "Ensino Superior":
        return "SUPERIOR"
    return None

def cadastrar_aluno(
    cpf: str,
    nome_completo: str,
    data_nasc: str,
    rua: str,
    numero: str,
    bairro: str,
    complemento: str,
    cidade: str,
    estado: str,
    celular: str,
    tel_contato: str,
    email: str,
    nivel_formacao: str
) -> None:
    """Calls database_manipulation.create_aluno() and displays a success or
    error message.

    Args:
        cpf (str): CPF of the person.
        nome_completo (str): Full name of the person.
        data_nasc (str): Date of birth of the person in DD/MM/YYYY format.
        rua (str): Street of the person's address.
        numero (str): Number of the person's address.
        bairro (str): Neighborhood of the person's address.
        complemento (str): Complement of the person's address.
        cidade (str): City of the person's address.
        estado (str): State of the person's address in short representation (2 chars).
        celular (str): Cellphone number of the person.
        tel_contato (str): Contact phone number of the person.
        email (str): Email of the person.
        nivel_formacao (str): Level of education of the person.
    """
    nivel_formacao = parse_nivel_formacao(nivel_formacao)
    try:
        create_aluno(
            cpf,
            nome_completo,
            data_nasc,
            rua,
            numero,
            bairro,
            complemento,
            cidade,
            estado,
            celular,
            tel_contato,
            email,
            nivel_formacao
        )
        st.success("Aluno cadastrado com sucesso!")
    except IntegrityError as e:
        st.error("CPF já cadastrado.")
    except InternalError as e:
        st.error("Erro interno. Tente novamente mais tarde, e contate o administrador se o erro persistir")
    except DatabaseError as e:
        st.error("Erro de banco de dados. Contate o administrador.")
    except InterfaceError as e:
        st.error("Erro de conexão com o banco de dados. Contate o administrador.")
    except Exception as e:
        st.error("Erro desconhecido. Contate o administrador.")

st.title("Cadastrar Aluno")

with st.form("form"):
    nome_completo = st.text_input("Nome completo")
    email = st.text_input("E-mail", placeholder="aluno@exemplo.com")
    col1, col2 = st.columns(2)
    cpf = col1.text_input("CPF", help="Somente números", placeholder="00000000000")
    data_nasc = col2.text_input("Data de nascimento", help="Formato: dd/mm/aaaa", placeholder="01/01/2000")
    celular = col1.text_input("Celular", help="Somente números", placeholder="11999999999")
    tel_contato = col2.text_input("Telefone de contato", help="Somente números", placeholder="11999999999")
    nivel_formacao = st.selectbox("Nível de formação", ["Ensino fundamental", "Ensino médio", "Ensino superior"])
    col1, col2 = st.columns([2, 1])
    rua = col1.text_input("Rua")
    numero = col2.text_input("Número", help="Somente números")
    col1, col2 = st.columns(2)
    complemento = col1.text_input("Complemento")
    bairro = col2.text_input("Bairro")
    cidade = col1.text_input("Cidade")
    estado = col2.text_input("Estado", help="Sigla do estado. Ex: SP", max_chars=2)
    submit_button = st.form_submit_button("Cadastrar")

    if submit_button:
        cadastrar_aluno(
            cpf,
            nome_completo,
            data_nasc,
            rua,
            numero,
            bairro,
            complemento,
            cidade,
            estado,
            celular,
            tel_contato,
            email,
            nivel_formacao
        )
